﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Enemy manager.
/// Class responsible for managing Enemy pools.
/// </summary>
public class EnemyManager : PoolManager<Enemy> {}
